import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthState } from 'src/modules/auth/src/lib/+state/auth.reducer';
import { getLoggedIn } from 'src/modules/auth/src/lib/+state/auth.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loggedIn$: Observable<boolean> = this.store.pipe(select(getLoggedIn));

  title = 'newProject';
  constructor(
    private store: Store<AuthState>
  ) {}
}
