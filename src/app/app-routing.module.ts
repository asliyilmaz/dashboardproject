import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AuthGuard } from 'src/modules/auth/src/lib/guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('src/modules/users/src/public-api').then((m) => m.UsersModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'home/ticket',
    loadChildren: () => import('src/modules/users/src/public-api').then((m) => m.UsersModule),
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),StoreModule.forRoot({}), ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
