import { ActionReducerMap } from '@ngrx/store';
import * as fromUsers from 'src/modules/users/src/lib/+state/users.reducer';

export interface State {
    user: fromUsers.UserState;
  }

  export const reducers: ActionReducerMap<State> = {
    user: fromUsers.reducer
  }