import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';
import { map, switchMap, catchError } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import * as UsersFeature from './users.reducer';
import * as UsersActions from './users.actions';
import { from, combineLatest, of, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
@Injectable()
export class UsersEffects {
  init$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.init),
    fetch({
      run: action => {
        // Your custom service 'load' logic goes here. For now just return a success action...
        return UsersActions.loadUsersSuccess({ users: [] });
      },

      onError: (action, error) => {
        console.error('Error', error);
        return UsersActions.loadUsersFailure({ error });
      }
    }))
  );
  GetUserInfo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.GetUserInfo),
      switchMap((action: ReturnType<typeof UsersActions.GetUserInfo>) => {
        return this.db
          .collection('userList')
          .snapshotChanges()
          .pipe(
            map((collectionActions: any) => {
              return collectionActions.map((collectionAction) => {
                return {
                  ...collectionAction.payload.doc.data()
                };
              });
            }),
            map((collectionList) => {
              return UsersActions.GetUserInfoLoaded({ payload: collectionList });
            }),
            catchError((error: any) => {
              return of(UsersActions.loadUsersFailure({ ...error, effect_name: 'GetUserInfo' }));
            })
          );
      })
    )
  );
  GetFeatureTs$ = createEffect(() =>
  this.actions$.pipe(
    ofType(UsersActions.GetFeatureTs),
    switchMap((action: ReturnType<typeof UsersActions.GetFeatureTs>) => {
      return this.db
        .collection('timeSeries')
        .doc(action.payload)
        .snapshotChanges()
        .pipe(
          map((collectionActions: any) => {
            return collectionActions.payload.data();
          }),
          map((collectionList) => {
            return UsersActions.GetFeatureTsLoaded({ payload: collectionList });
          }),
          catchError((error: any) => {
            return of(UsersActions.loadUsersFailure({ ...error, effect_name: 'GetFeatureTs' }));
          })
        );
    })
  )
);


  constructor(
    private actions$: Actions,
    private db: AngularFirestore

  ) { }
}
