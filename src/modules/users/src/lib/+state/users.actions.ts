import { createAction, props } from '@ngrx/store';
import { UsersEntity } from './users.models';

export const init = createAction(
  '[Users Page] Init'
);

export const loadUsersSuccess = createAction(
  '[Users/API] Load Users Success',
  props<{ users: UsersEntity[] }>()
);

export const loadUsersFailure = createAction(
  '[Users/API] Load Users Failure',
  props<{ error: any }>()
);
export const GetUserInfo = createAction('[Users/API] Get User Info');
export const GetUserInfoLoaded = createAction('[Users/API] Get User Info Loaded', props<{ payload: any }>());

export const GetFeatureTs = createAction('[Users/API] Get Feature Ts', props<{ payload: any }>());
export const GetFeatureTsLoaded = createAction('[Users/API] Get Feature Ts Loaded', props<{ payload: any }>());


