import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as UsersActions from './users.actions';
import { UsersEntity } from './users.models';

export const USERS_FEATURE_KEY = 'users';

export interface UserState extends EntityState<UsersEntity> {
  selectedId ?: string | number;          // which Users record has been selected
  loaded      : boolean;                  // has the Users list been loaded
  error      ?: string | null;            // last known error (if any)
  usersInfo?: any;
  featureTs?: any;
}

export interface UsersPartialState {
  readonly [USERS_FEATURE_KEY]: UserState;
}

export const usersAdapter: EntityAdapter<UsersEntity> = createEntityAdapter<UsersEntity>();

export const initialState: UserState = usersAdapter.getInitialState({
  // set initial required properties
  loaded : false
});

const usersReducer = createReducer(
  initialState,
  on(UsersActions.init,
    state => ({ ...state, loaded: false, error: null })
  ),
  on(UsersActions.loadUsersSuccess,
    (state, { users }) => usersAdapter.setAll(users, { ...state, loaded: true })
  ),
  on(UsersActions.loadUsersFailure,
    (state, { error }) => ({ ...state, error })
  ),
  on(UsersActions.GetUserInfoLoaded, (state, action) => {
    return {
      ...state,
      usersInfo: action.payload
    };
  }),
  on(UsersActions.GetFeatureTsLoaded, (state, action) => {
     return {
       ...state,
       featureTs: action.payload
     };
   }),
);

export function reducer(state: UserState | undefined, action: Action) {
  return usersReducer(state, action);
}
