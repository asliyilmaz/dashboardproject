import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { GetUserInfo } from '../../+state/users.actions';
import { UserState } from '../../+state/users.reducer';

@Component({
  selector: 'lib-ticket-container',
  templateUrl: './ticket-container.component.html',
  styleUrls: ['./ticket-container.component.scss']
})
export class TicketContainerComponent implements OnInit {

  constructor(
    private store: Store<UserState>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(GetUserInfo());
  }

}
