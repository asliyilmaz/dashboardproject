import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getLoggedInAndVerified } from 'src/modules/auth/src/lib/+state/auth.selectors';
import { GetFeatureTs } from '../../+state/users.actions';
import { UserState } from '../../+state/users.reducer';
import { getFeatureTs } from '../../+state/users.selectors';

@Component({
  selector: 'lib-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.scss']
})
export class DashboardContainerComponent implements OnInit {
  featureTs$: Observable<any>;

  constructor(
    private store: Store<UserState>,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.store.pipe(select(getLoggedInAndVerified)).subscribe(loggedIn => {
      if (loggedIn) {
        const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/home';
        const parsedUrl = this.router.parseUrl(this.route.snapshot.queryParamMap.get('returnUrl') || '/home');
        let queryParams = parsedUrl.queryParams;

        const returnUrlRoot = returnUrl.split('?')[0];
        this.router.navigate([returnUrlRoot], { queryParams: queryParams });
      } else {
        this.router.navigate(['/login'])
      }
    });
    this.store.dispatch(GetFeatureTs({ payload: 'Unresolved' }))

    this.featureTs$ = this.store.pipe(select(getFeatureTs));
  }
  handleFeatureClick(event) {
    this.store.dispatch(GetFeatureTs({ payload: event }))
  }
}
