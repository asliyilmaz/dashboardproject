import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromUsers from './+state/users.reducer';
import { UsersEffects } from './+state/users.effects';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UsersComponent } from './components/users/users.component';
import { DashboardContainerComponent } from './container/dashboard-container/dashboard-container.component';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { UserListTableComponent } from './components/user-list-table/user-list-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { TicketContainerComponent } from './container/ticket-container/ticket-container.component';
import { TimeSeriesComponent } from './components/time-series/time-series.component';
import { PlotlyModule } from 'angular-plotly.js';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import {MatRadioModule} from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatBadgeModule} from '@angular/material/badge';
import { TopbarComponent } from './components/topbar/topbar.component';
PlotlyModule.plotlyjs = PlotlyJS;
@NgModule({
  declarations: [SidebarComponent,UsersComponent,DashboardContainerComponent, UserListTableComponent, TicketContainerComponent, TimeSeriesComponent, TopbarComponent],
  imports: [
  CommonModule,
  MatIconModule,
  MatTableModule,
  MatButtonModule,
  MatCardModule,
  MatRadioModule,
  MatPaginatorModule,
  PlotlyModule,
  MatBadgeModule,
  StoreModule.forFeature(fromUsers.USERS_FEATURE_KEY, fromUsers.reducer),
  EffectsModule.forFeature([UsersEffects]),
  RouterModule.forChild([
    { path: 'home', pathMatch: 'full', component: DashboardContainerComponent },
    { path: 'home/ticket', pathMatch: 'full', component: TicketContainerComponent }
  ])],
  exports: [PlotlyModule,SidebarComponent,UsersComponent,TimeSeriesComponent,TopbarComponent] 
})
export class UsersModule { }
