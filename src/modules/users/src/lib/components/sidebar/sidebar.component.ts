import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  pageList = [
    {
      name: 'Overview',
      icon: 'fa fa-pie-chart',
      link: 'home'
    },
    {
      name: 'Tickets',
      icon: 'fa fa-ticket',
      link: 'home/ticket'
    },
    {
      name: 'Ideas',
      icon: 'fa fa-lightbulb-o',
      link: 'ideas'
    },
    {
      name: 'Contacts',
      icon: 'fa fa-users',
      link: 'contacts'
    },

    {
      name: 'Agents',
      icon: 'fa fa-user',
      link: 'agents'
    },
    {
      name: 'Articles',
      icon: 'fa fa-book',
      link: 'articles'
    }
  ];
  settings = [
    {
      name: 'Settings',
      icon: 'fa fa-cog',
      link: 'settings'
    },
    {
      name: 'Subscription',
      icon: 'fa fa-address-card-o',
      link: 'subscription'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
