import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
export interface PeriodicElement {
  ticketDetails?: string;
  customerName?: string;
  date?: string;
  priority?: any;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {priority: 'HIGH', ticketDetails: 'Contact Email not Linked', customerName: 'Tom Cruise', date: 'May 26, 2019'},
  {priority: 'LOW', ticketDetails: 'Adding Images to Featured Posts', customerName: 'Mat Damon', date: 'May 26, 2019'},
  {priority:'HIGH', ticketDetails: 'When will I be charged this month?', customerName: 'Rober Downey', date: 'May 26, 2019'},
  {priority: 'NORMAL', ticketDetails: 'Payment not going througt', customerName: 'Christian Bale', date: 'May 25, 2019'},
  {priority: 'HIGH', ticketDetails: 'Unable to add replies', customerName: 'Henry Cavil', date: 'May 25, 2019'},
  {priority: 'NORMAL', ticketDetails: 'Downtime since last week', customerName: 'Chris Evans', date: 'May 25, 2019'},
  {priority: 'LOW', ticketDetails: 'Referral Bonus', customerName: 'Sam Smith', date: 'May 25, 2019'},
  {priority: 'NORMAL', ticketDetails: 'How do I change my password?', customerName: 'Steve Rogers', date: 'May 24, 2019'}
];
@Component({
  selector: 'lib-user-list-table',
  templateUrl: './user-list-table.component.html',
  styleUrls: ['./user-list-table.component.scss']
})


export class UserListTableComponent implements OnInit {
  displayedColumns: string[] = ['ticketDetails', 'customerName', 'date', 'priority', 'star'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
