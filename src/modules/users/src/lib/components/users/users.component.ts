import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'lib-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @Output() featureClick = new EventEmitter();
  upArea = [
    { title: 'Unresolved', value: 60 },
    { title: 'Overdue', value: 16 },
    { title: 'Open', value: 43 },
    { title: 'Onhold', value: 64 }

  ];
  middleArea = [
    { title: 'Resolved', value: 449 },
    { title: 'Received', value: 426 },
    { title: 'Average first response time', value: '33m' },
    { title: 'Average response time', value: '3h 8m' },
    { title: 'Resolution within SLA', value: '94%' },

  ];
  downAreaLeft= [
    { title: 'Waiting on Feature Request', value: 4238 },
    { title: 'Awaiting Customer Response', value: 1005 },
    { title: 'Awaiting Developer Fix', value: 914 },
    { title: 'Pending', value: 218 }
  ]
  downAreaRight= [
    { title: 'Finish ticket update', button: 'Urgent' },
    { title: 'Create new ticket example', button: 'New' },
    { title: 'Update ticket report', button: 'Default' }
  ]
  _tsdata;
  @Input()
  set data(value) {
    if (!value) return;
    this._tsdata = value;
  }
  get data() {
    return this._tsdata;
  }
  constructor() { }

  ngOnInit(): void {
  }
  handleFeatureClick(event){
    this.featureClick.emit(event);
  }
}
