import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'lib-time-series',
  templateUrl: './time-series.component.html',
  styleUrls: ['./time-series.component.scss']
})
export class TimeSeriesComponent implements OnInit {
  _data;
  stockName;
  @Output() stockNameClicked = new EventEmitter<any>();
  @Input()
  set data(value: any) {
    if (!value) return;
    
    this._data = value;
    this.stockName = this._data.name;
    this.updateData(this._data);
  
  }
  constructor() {}

  ngOnInit(): void {
  }
  ts = {
    data: [],
    layout: {
      margin: {
        t: 50,
        b: 100,
        l: 20,
        r: 20,
        pad: 0,
      },
      autosize: true,
      legend: {
        orientation: 'v',
        traceorder: 'normal',
      },
      font: {
        size: 11,
      },
      title: {
        text: '',
        font: '14px',
      },

      xaxis: {
        automargin: true,
        autorange: true,
        range: [],
        showgrid: false
      },
      yaxis: {
        automargin: true,
        autorange: true,
        range: [],
        title: {
          text: '<b></b>',
        }
      },
    },

    config: {
      showLink: false,
      displaylogo: false,
      modeBarButtons: [[], ['toImage', 'autoScale2d']],
    },
  };
  updateData(data: { x: number[]; y: string[]; xToday: number[]; yToday: string[] }) {
    var yesterday = {
      legendgroup: 'yesterday',
      name: 'yesterday',
      x: data.x,
      y: data.y,
      type: 'linear',
      line: {'shape': 'spline', 'smoothing': 1.3},
      marker: {
        color: '#E0E0E4',
      }
    };
    var today = {
      legendgroup: 'today',
      name: 'today',
      x: data.xToday,
      y: data.yToday,
      type: 'linear',
      fill: 'tozeroy',
      line: {'shape': 'spline', 'smoothing': 1.3},
      marker: {
        color: '#CCCCFF',
      }
    };

    this.ts.data = [yesterday,today];
  }
}
