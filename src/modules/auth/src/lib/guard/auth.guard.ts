import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';
import { AuthState } from '../+state/auth.reducer';
import { getLoggedIn } from '../+state/auth.selectors';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AuthState>, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log("sdasdfd")
      return this.store.pipe(
        select(getLoggedIn),
        tap((loggedIn: boolean) => {
          if (!loggedIn) {
            this.router.navigate(['/login']);
            return false;
          }else{
            this.router.navigate(['/home']);
            return true;
          }
        })
      );
  }
  
}
