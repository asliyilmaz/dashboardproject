import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as AuthActions from './auth.actions';
import { AuthEntity } from './auth.models';

export const AUTH_FEATURE_KEY = 'auth';

export interface AuthState extends EntityState<AuthEntity> {
  selectedId ?: string | number;          // which Auth record has been selected
  loaded      : boolean;                  // has the Auth list been loaded
  error      ?: string | null;            // last known error (if any)
  loggedIn?: boolean;
  verified?: boolean;
}

export interface AuthPartialState {
  readonly [AUTH_FEATURE_KEY]: AuthState;
}

export const authAdapter: EntityAdapter<AuthEntity> = createEntityAdapter<AuthEntity>();

export const initialState: AuthState = authAdapter.getInitialState({
  // set initial required properties
  loaded : false
});

const authReducer = createReducer(
  initialState,

  on(AuthActions.EmailLoginSuccess, (state, action) => {
     return {
       ...state,
       verified: action.emailVerified,
       loggedIn: true
     };
   })
);

export function reducer(state: AuthState | undefined, action: Action) {
  return authReducer(state, action);
}
