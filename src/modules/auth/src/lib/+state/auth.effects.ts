import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';

import * as AuthFeature from './auth.reducer';
import * as AuthActions from './auth.actions';
import { catchError, concatMap, map, switchMap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Alert, AlertType } from '../models/alert.model'
import { AlertService } from '../services/alert.service'
import { Router } from '@angular/router';
@Injectable()
export class AuthEffects {
  init$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.init),
    fetch({
      run: action => {
        // Your custom service 'load' logic goes here. For now just return a success action...
        return AuthActions.loadAuthSuccess({ auth: [] });
      },

      onError: (action, error) => {
        console.error('Error', error);
        return AuthActions.loadAuthFailure({ error });
      }
    }))
  );
  initAuthListener$ = this.auth.authState.pipe(
    map((user) => {
      return !!user ? AuthActions.SetAuthenticated({ payload: { emailVerified: user.emailVerified } }) : AuthActions.SetNotAuthenticated();
    })
  );
  EmailLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.EmailLogin),
      switchMap((action: ReturnType<typeof AuthActions.EmailLogin>) => {
        const { email, password } = action.payload;
        return from(this.auth.auth.signInWithEmailAndPassword(email, password)).pipe(
          concatMap((userCred) => {
            if (userCred.user.emailVerified) {
              AuthActions.SetAuthenticated({ payload: { emailVerified: userCred.user.emailVerified } });
              return [AuthActions.EmailLoginSuccess({ emailVerified: userCred.user.emailVerified }), AuthActions.CheckUserVerificationFailed({ email: email })];

            } else {
              const message = new Alert('Email or password invalid',AlertType.Success);
              this.alertService.show(message);
              return [AuthActions.loadAuthSuccess({ auth: [] })];
            }
          })
        );
      }),
      catchError((error: any) => {
        console.log(error)
        const message = new Alert(error.message ? error.message : JSON.parse(error.message)?.error.message, AlertType.Success);
        this.alertService.show(message);
        return of(AuthActions.loadAuthFailure({ ...error, effect_name: 'CheckUserPassword' }));
      })
    )
  );






  constructor(
    private actions$: Actions,
    private auth: AngularFireAuth,
    private db: AngularFirestore,
    private alertService: AlertService,
    private router: Router
  ) { }
}
