import { createAction, props } from '@ngrx/store';
import { AuthEntity } from './auth.models';
import { Credential } from '../models/credential.model';

export const init = createAction(
  '[Auth Page] Init'
);

export const loadAuthSuccess = createAction(
  '[Auth/API] Load Auth Success',
  props<{ auth: AuthEntity[] }>()
);

export const loadAuthFailure = createAction(
  '[Auth/API] Load Auth Failure',
  props<{ error: any }>()
);
export const EmailLogin = createAction('[Auth/API] Email Login',props<{ payload:Credential }>());
export const EmailLoginSuccess = createAction('[Auth/API] Email Login Success',props<{ emailVerified: boolean }>());

export const CheckUserVerificationFailed = createAction('[Auth/API] Check User Verificatio Failed',props<{ email: string }>());

export const SetAuthenticated = createAction('[Auth/API] Set Authenticated',props<{ payload:{emailVerified: boolean }}>());
export const SetNotAuthenticated = createAction('[Auth/API] Set Not Authenticated');

