import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { EmailLogin } from '../../+state/auth.actions';
import { AuthState } from '../../+state/auth.reducer';
import { getLoggedInAndVerified } from '../../+state/auth.selectors';
import { Credential} from '../../models/credential.model'
@Component({
  selector: 'app-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.scss']
})
export class LoginContainerComponent implements OnInit {

  constructor(private store: Store<AuthState>,private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    this.store.pipe(select(getLoggedInAndVerified)).subscribe(loggedIn => {
      if (loggedIn) {
        const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/home';
        const parsedUrl = this.router.parseUrl(this.route.snapshot.queryParamMap.get('returnUrl') || '/home');
        let queryParams = parsedUrl.queryParams;
  
        const returnUrlRoot = returnUrl.split('?')[0];
        this.router.navigate([returnUrlRoot], { queryParams: queryParams });
      }
    });
  }

  login(credential: Credential) {
    this.store.dispatch(EmailLogin({payload:credential}));
  }
}
