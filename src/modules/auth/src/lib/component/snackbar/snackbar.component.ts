import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Alert } from '../../models/alert.model';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {


  ngOnInit(): void {
  }
  @Output() closeSnackBar: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: Alert) {}

  close() {
    this.closeSnackBar.emit(true);
  }
}
