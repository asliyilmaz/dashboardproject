import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  @Output() loginClicked = new EventEmitter<Credential>();
  hide = true;
  constructor(private fb: FormBuilder) {
    this.loginForm = fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [
        null,
        Validators.compose([
          Validators.minLength(6),
          Validators.required
        ]),
      ],
    });
   }

  ngOnInit(): void {
  }
  handleLoginClick() {
    this.loginClicked.next(this.loginForm.value);
  }

}
