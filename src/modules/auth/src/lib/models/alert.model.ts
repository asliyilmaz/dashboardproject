export enum AlertType {
    Danger = 'Danger',
    Failed = 'Failed',
    Success = 'Success'
  }
  
  export class Alert {
    text: string;
    type: AlertType;
  
    constructor(text, type?) {
      this.text = text;
      this.type = type;
    }
  }