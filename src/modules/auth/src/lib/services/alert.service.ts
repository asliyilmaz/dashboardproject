import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Alert } from '../models/alert.model';
import { SnackbarComponent } from '../component/snackbar/snackbar.component'

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  snackBarSubscription: Subscription;
  constructor(private snackBar: MatSnackBar) { }
  show(alert: Alert, duration = 6000) {
    const snackBarRef = this.snackBar.openFromComponent(SnackbarComponent, {
      data: alert,
      duration: duration,
      verticalPosition: 'top'
    });

    this.snackBarSubscription = snackBarRef.instance.closeSnackBar.subscribe((event: boolean) => {
      if (event) {
        snackBarRef.dismiss();
      }
    });
  }
  ngOnDestroy(): void {
    this.snackBarSubscription.unsubscribe();
  }
}
