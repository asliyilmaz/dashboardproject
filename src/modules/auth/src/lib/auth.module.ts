import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromAuth from './+state/auth.reducer';
import { AuthEffects } from './+state/auth.effects';
import { LoginComponent } from './component/login/login.component';
import { LoginContainerComponent } from './container/login-container/login-container.component';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SnackbarComponent } from './component/snackbar/snackbar.component';


@NgModule({
  declarations: [AuthComponent, LoginComponent, LoginContainerComponent, SnackbarComponent],
  imports: [
  MatFormFieldModule,
  BrowserModule,
  ReactiveFormsModule,
  MatInputModule,
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatSnackBarModule,
  FormsModule,
  StoreModule.forFeature(fromAuth.AUTH_FEATURE_KEY, fromAuth.reducer),
  EffectsModule.forFeature([AuthEffects]),
  RouterModule.forChild([
    {
      path: 'auth',
      children: [
        {
          path: '',
          component: LoginContainerComponent,
        }
      ],
    },
    { path: 'login', pathMatch: 'full', component: LoginContainerComponent },
    { path: '', pathMatch: 'full', component: LoginContainerComponent }

  ])
],
  
  exports: [AuthComponent,LoginComponent,LoginContainerComponent,SnackbarComponent]
})
export class AuthModule { }
