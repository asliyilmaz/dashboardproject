// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA-KfgKXP8Txf5C-uViLT4reekZWbWxVEk",
    authDomain: "new-project-ff515.firebaseapp.com",
    projectId: "new-project-ff515",
    storageBucket: "new-project-ff515.appspot.com",
    messagingSenderId: "171174080224",
    appId: "1:171174080224:web:2c6c99cfa214eb96036a85",
    measurementId: "G-BQW6E98DRJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
